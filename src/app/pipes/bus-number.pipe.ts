import { Pipe, PipeTransform } from '@angular/core';
import { Bus } from '../models/bus';

@Pipe({
  name: 'busNumber'
})
export class BusNumberPipe implements PipeTransform {

  transform(busses: Array<Bus>, busNumber: string): any {
    if (busNumber == 'all') {
      return busses;
    } else {
      return busses.filter(x => x.DISPLAY_ROUTE_CODE == busNumber);
    }
  }

}
