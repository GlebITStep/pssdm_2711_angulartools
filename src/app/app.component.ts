import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: [ './app.component.scss' ]
})
export class AppComponent {
  title = 'FirstApp';
  num1 = 5;
  num2 = 3;
  image = 'https://s3.amazonaws.com/explodingkittens.com/img/misc/name_gen_kitty_x1.png';

  onButtonClick(): void {
    this.title = 'Clicked!';
  }
}
