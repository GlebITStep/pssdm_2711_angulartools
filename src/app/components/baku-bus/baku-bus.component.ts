import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Bus } from 'src/app/models/bus';

@Component({
  selector: 'app-baku-bus',
  templateUrl: './baku-bus.component.html',
  styleUrls: ['./baku-bus.component.scss']
})
export class BakuBusComponent implements OnInit {
  lat = 40.414897;
  lon = 49.8511233;
  busses = new Array<Bus>();
  busNumber = 'all';
  busNumbers = new Array<string>();

  constructor(private httpClient: HttpClient) { }

  async ngOnInit() {
    this.busNumbers = await this.httpClient.get<Array<string>>('https://localhost:44345/api/bakubus/busNumbers').toPromise();

    await this.getBusses();

    setInterval(async () => {
      await this.getBusses();
    }, 10000);
  }

  async getBusses() {
    this.busses = await this.httpClient.get<Array<Bus>>('https://localhost:44345/api/bakubus/busses').toPromise();
    console.log(this.busses); 
  }

}
