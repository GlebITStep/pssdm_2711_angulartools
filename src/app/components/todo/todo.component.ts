import { Component, OnInit } from '@angular/core';
import { Task } from '../../models/task';

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.scss']
})
export class TodoComponent implements OnInit {
  tasks = new Array<Task>();
  newTask = new Task();
  editMode = false;

  constructor() { }

  ngOnInit() {
    this.tasks.push(new Task('One', 'Lorem ipsum'));
    this.tasks.push(new Task('Two', 'Dolor sit amet'));
    this.tasks.push(new Task('Three', 'Hello!'));
  }

  onFormSubmit(): void {
    this.tasks.push(this.newTask);
    this.newTask = new Task();
  }

  onTaskClick(item: Task): void {
    item.done = !item.done;
  }

  onTaskDelete(item: Task): void {
    this.tasks = this.tasks.filter(x => x != item);
  }

  onTaskEdit(item: Task): void {
    this.newTask = item;
    this.editMode = true;
  }

  onDoneClick():void {
    this.newTask = new Task();
    this.editMode = false;
  }
}