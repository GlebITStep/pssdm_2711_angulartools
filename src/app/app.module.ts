import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';

import { AgmCoreModule } from '@agm/core'

import { AppComponent } from './app.component';
import { CounterComponent } from './components/counter/counter.component';
import { TodoComponent } from './components/todo/todo.component';
import { ConverterComponent } from './components/converter/converter.component';
import { DigitsPipe } from './pipes/digits.pipe';
import { BakuBusComponent } from './components/baku-bus/baku-bus.component';
import { BusNumberPipe } from './pipes/bus-number.pipe';

@NgModule({
  declarations: [
    AppComponent,
    CounterComponent,
    TodoComponent,
    ConverterComponent,
    DigitsPipe,
    BakuBusComponent,
    BusNumberPipe
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyDSfSWevewO_tWAt9_eZKkKLwk6VHKHx3g'
    })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
